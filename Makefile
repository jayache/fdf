# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jayache <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/17 14:14:36 by jayache           #+#    #+#              #
#    Updated: 2019/09/04 11:26:29 by jayache          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

MAIN = main.c
S = screen.c ft_draw_point.c ft_draw_lign.c pixel.c pipeline.c error_handling.c \
   parser.c helper.c draw_all_vectors.c hooks.c get_vector.c print_debug.c	
MO = $(MAIN:.c=.o)
O = $(S:.c=.o)
L = libft.a
I = fdf.h

TARGET = fdf
FLAGS = -Wextra -Wall -g3 -fsanitize=address 
LINKER = -lmlx -lm -framework OpenGL -framework AppKit

LIBDIR = lib/
OBJDIR = obj/
SRCDIR = src/
INCDIR = inc/

OBJ := $(addprefix $(OBJDIR), $(O))
MOBJ := $(addprefix $(OBJDIR), $(MO))
SRC := $(addprefix $(SRCDIR), $(S))
LIB := $(addprefix $(LIBDIR), $(L))
INC := $(addprefix $(INCDIR), $(I))
DEP = $(OBJ:%.o=%.d) $(MOBJ:%.o=%.d)

all:
	@mkdir -p $(OBJDIR)	
	@make -C $(LIBDIR) 
	@make $(TARGET)

errorlibft:
	@echo "The libft recipe failed, you stoopid."
	@exit

$(OBJDIR)%.o: $(SRCDIR)%.c
	@mkdir -p $(OBJDIR)
	@gcc -MMD -c $(FLAGS) $< -I $(INCDIR) -o $@ 
	@echo "Creating $@"

$(TARGET): $(LIB) $(OBJ) $(MOBJ) $(INC)
	gcc $(OBJ) $(MOBJ) $(FLAGS) $(LIB) -o $(TARGET) $(LINKER)

$(LIB):
	make -C $(LIBDIR)

clean:
	@rm -rf $(OBJ) $(UOBJ) $(MOBJ) $(DEP)
	@make clean -C $(LIBDIR) 

fclean: clean
	@rm -rf $(UTARGET) $(TARGET)
	@make fclean -C $(LIBDIR)

re: fclean all

-include $(DEP)

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 23:11:11 by jayache           #+#    #+#             */
/*   Updated: 2019/09/04 10:35:48 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FUNCTIONS_H
# define FUNCTIONS_H

/*
** DRAWING FUNCTIONS
** GOAL: DRAW STUFF ON THE SCREEN
*/

t_pixel				ft_pixel(unsigned char red, unsigned char green,
		unsigned char blue);
void				ft_draw_point(t_screen *screen, t_vector3 coor,
		t_pixel pixel);
void				ft_draw_lign(t_screen *screen, t_vector3 begin,
		t_vector3 end, t_pixel pixel);

/*
** INITIALISATION
** GOAL: SETUP THE PROGRAM
*/

t_screen			init_screen(unsigned short width, unsigned short height);

/*
** ERROR HANDLING
** GOAL: HANDLE ERROR CASES GRACEFULLY (I.E. WITHOUT SEGFAULTS)
*/

void		exit_error(char const *const msg);
int			help(void);

/*
** PARSER
** GOAL: TRANSFORM INPUT ARGUMENTS INTO USEFUL DATA
*/

void		get_vector_array(t_screen *screen, char *filename);
int			is_valid_number(char *buffer);

/*
** VECTOR TRANSMATION
** GOAL: TRANSFORM A VECTOR FROM WORLD-SPACE TO SCREEN-SPACE
*/

t_vector3	world_to_screen(t_screen *screen, t_vector4 vtc);

/*
** MASS VECTORS MANIPULATION
** GOAL: UPDATE AND DRAW EVERY VECTORS
*/

void		process_new_matrix(t_camera *camera);
void		update_all_vectors(t_screen *screen);
void		draw_all_vectors(t_screen *screen);
void		clean_zbuffer(t_screen *screen);
int			get_vector(t_screen *screen, t_vector2 vectofind);
t_vector3	*get_next_vector(t_screen *s);
t_vector3	*get_next_tvector(t_screen *s);

/*
** HOOKS
** GOAL: PROVIDE A RESPONSIVE PROGRAM
*/

int			key_hook(int key, void *screen);

/*
** PRINT FOR DEBUG
** GOAL: PROVIDE EASY WAY TO DUMP PROGRAM CONTENT
*/

void		print_screen(t_screen *s);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/06 15:26:43 by jayache           #+#    #+#             */
/*   Updated: 2019/09/02 20:43:51 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct		s_camera
{
	float		ratio;
	int			fov;
	int			near;
	int			far;
	t_vector3	origin;
	t_matrix	orient_matrix;
	t_matrix	origin_matrix;
	t_matrix	view_matrix;
	t_matrix	projection_matrix;
}					t_camera;

typedef struct		s_screen
{
	unsigned short	width;
	unsigned short	height;
	int				array_width;
	int				array_height;
	t_vector3		**vectors;
	t_vector3		**tvectors;
	long			*zbuffer;
	int				bitsperpixel;
	int				endian;
	int				size_line;
	char			*screen;
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	t_camera		camera;
}					t_screen;

typedef struct		s_pixel
{
	unsigned char	red;
	unsigned char	blue;
	unsigned char	green;
}					t_pixel;

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_all_vectors.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 11:19:20 by jayache           #+#    #+#             */
/*   Updated: 2019/09/04 11:28:27 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		update_all_vectors(t_screen *screen)
{
	t_vector3	*vtc;
	t_vector3	*tvtc;

	while ((vtc = get_next_vector(screen)) && (tvtc = get_next_tvector(screen)))
	{
		*tvtc = world_to_screen(screen,
					ft_vector4_from_3(*vtc));
	}
	get_next_tvector(screen);
}

//TO UPDATE
void		draw_all_vectors(t_screen *screen)
{
	t_vector3	*vec;
	int	up;
	int down;
	int right;
	int left;

	while ((vec = get_next_tvector(screen)))
	{
/*		left = get_vector(screen, ft_vector2(screen->vectors[i].x - 1, screen->vectors[i].z));
		right = get_vector(screen, ft_vector2(screen->vectors[i].x + 1, screen->vectors[i].z));
		down = get_vector(screen, ft_vector2(screen->vectors[i].x, screen->vectors[i].z + 1));
		up = get_vector(screen, ft_vector2(screen->vectors[i].x, screen->vectors[i].z - 1));
		if (up != -1)
			ft_draw_lign(screen, screen->tvectors[i], screen->tvectors[up], ft_pixel(0, 255, 0));
		if (down != -1)
			ft_draw_lign(screen, screen->tvectors[i], screen->tvectors[down], ft_pixel(0, 255, 0));
		if (right != -1)
			ft_draw_lign(screen, screen->tvectors[i], screen->tvectors[right], ft_pixel(0, 255, 0));
		if (left != -1)
			ft_draw_lign(screen, screen->tvectors[i], screen->tvectors[left], ft_pixel(0, 255, 0));
*/
		if (vec->x != -1)
		ft_draw_point(screen, *vec, ft_pixel(0, 0, 255));
	}
}

void		clean_zbuffer(t_screen *screen)
{
	int	i;

	i = 0;
	while (screen->width * screen->height > i)
	{
		screen->zbuffer[i] = 0;
		screen->screen[i * 4] = 0;
		screen->screen[i * 4 + 1] = 0;
		screen->screen[i * 4 + 2] = 0;
		screen->screen[i * 4 + 3] = 0;
		++i;
	}
}

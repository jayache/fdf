/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handling.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 16:24:38 by jayache           #+#    #+#             */
/*   Updated: 2019/08/31 16:34:16 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		help(void)
{
	ft_printf("./fdf [filename]\n");
	return (1);
}

void	exit_error(char const *const msg)
{
	ft_printf("%s\n", msg);
	exit(1);
}

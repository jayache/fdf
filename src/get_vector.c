/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_vector.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 14:45:04 by jayache           #+#    #+#             */
/*   Updated: 2019/09/02 21:23:07 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

//need to decide on an error value

int			get_vector(t_screen *s, t_vector2 vectofind)
{
	if (vectofind.x < 0 || vectofind.y < 0 || vectofind.x > s->array_width
			|| vectofind.y >= s->array_height)
		return (-1);
	if (s->vectors[vectofind.y][vectofind.x].x != -1)
		return (5);
	else
		return (-1);
}

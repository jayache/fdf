/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 17:26:42 by jayache           #+#    #+#             */
/*   Updated: 2019/09/04 10:44:38 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	is_valid_number(char *number)
{
	int	i;

	i = 0;
	while (number[i] && !ft_iswhitespace(number[i]))
	{
		if (!ft_isdigit(number[i]))
			if (((number[i] != '-' && number[i] != '+') || i != 0)
					|| !ft_iswhitespace(number[i + 1]))
				return (0);
		++i;
	}
	return (i < 9 ? 1 : 0);
}

t_vector3	*get_next_vector(t_screen *s)
{
	static int	width;
	static int	height;
	t_vector3	*val;
	
	if (height >= s->array_height)
	{
		height = 0;
		return (NULL);
	}
	val = &(s->vectors[height][width]);
	width++;
	if (width >= s->array_width)
	{
		height++;
		width = 0;
	}
	if (val->x != -1)
		return (val);
	else
		return (get_next_vector(s));
}

t_vector3	*get_next_tvector(t_screen *s)
{
	static int	width;
	static int	height;
	t_vector3	*val;
	t_vector3	*vval;

	if (height >= s->array_height)
	{
		height = 0;
		return (NULL);
	}
	val = &(s->tvectors[height][width]);
	vval = &(s->vectors[height][width]);
	width++;
	if (width >= s->array_width)
	{
		height++;
		width = 0;
	}
	if (vval->x != -1)
		return (val);
	else
		return (get_next_tvector(s));
}

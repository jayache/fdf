/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 11:39:20 by jayache           #+#    #+#             */
/*   Updated: 2019/09/01 14:30:19 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	key_hook(int key, void *screen)
{
	t_screen *s;

	s = screen;
	ft_printf("%d\n", key);
	if (key == 126)
		s->camera.origin = ft_vector3(s->camera.origin.x, s->camera.origin.y - 5, s->camera.origin.z);
	if (key == 125)
		s->camera.origin = ft_vector3(s->camera.origin.x, s->camera.origin.y + 5, s->camera.origin.z);
	if (key == 124)
		s->camera.origin = ft_vector3(s->camera.origin.x + 5, s->camera.origin.y, s->camera.origin.z);
	if (key == 123)
		s->camera.origin = ft_vector3(s->camera.origin.x - 5, s->camera.origin.y, s->camera.origin.z);
	if (key == 6)
		s->camera.origin = ft_vector3(s->camera.origin.x, s->camera.origin.y, s->camera.origin.z - 5);
	if (key == 7)
		s->camera.origin = ft_vector3(s->camera.origin.x, s->camera.origin.y, s->camera.origin.z + 5);
	process_new_matrix(&(s->camera));
	update_all_vectors(s);
	draw_all_vectors(s);
	mlx_put_image_to_window(s->mlx_ptr, s->win_ptr, s->img_ptr, 0, 0);
	clean_zbuffer(s);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/06 15:12:17 by jayache           #+#    #+#             */
/*   Updated: 2019/09/04 10:59:13 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "fdf.h"

t_vector3	**get_transformed_array(t_screen *screen)
{
	t_vector3	**tarray;
	int			i;

	i = 0;
	tarray = (t_vector3**)ft_memalloc(screen->array_height * sizeof(t_vector3*));
	while (i < screen->array_height)
	{
		tarray[i] = (t_vector3*)ft_memalloc(screen->array_width * sizeof(t_vector3));
		++i;
	}	
	return (tarray);
}

static void	initialization(t_screen *screen, int ac, char **av)
{
	if (ac != 2)
		exit(help());
	*screen = init_screen(500, 500);
	get_vector_array(screen, av[1]);
	screen->tvectors = get_transformed_array(screen);
	update_all_vectors(screen);
	draw_all_vectors(screen);
}

int main(int argc, char **argv)
{
	t_screen screen;

	initialization(&screen, argc, argv);
	mlx_put_image_to_window(screen.mlx_ptr, screen.win_ptr, screen.img_ptr, 0, 0);
	mlx_key_hook(screen.win_ptr, key_hook, &screen);
	mlx_loop(screen.mlx_ptr);

}

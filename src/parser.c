/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 16:24:02 by jayache           #+#    #+#             */
/*   Updated: 2019/09/04 10:28:21 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** FILL IN AND RETURN WIDTH OF THE BIGGEST LINE, NUMBER OF LINE AND NUMBERS OF
** POINTS
*/

static int	get_number_of_vec(int fd, int *wd, int *ht)
{
	int		cnt;
	char	*buffer;
	int		i;
	int		status;
	int		line;

	cnt = 0;
	status = 0;
	while (get_next_line(fd, &buffer))
	{
		line = 0;
		i = 0;
		while (i == 0 || buffer[i - 1])
		{
			if (!ft_iswhitespace(buffer[i]))
			{
				cnt = (status == 0) ? cnt + 1 : cnt;
				line = (status == 0) ? line + 1 : line;
				status = 1;
			}
			else
				status = 0;
			++i;
		}
		*wd = *wd < line ? line : *wd;
		*ht += 1;
		free(buffer);
	}
	return (cnt);
}

static void	fill_line(t_vector3 *array, char *buffer, int pos, int y)
{
	int	i;
	int	status;
	int	x;

	x = 0;
	i = 0;
	status = 0;
	while (i == 0 || buffer[i - 1])
	{
		if (!ft_iswhitespace(buffer[i]))
		{
			if (status == 0)
			{
				if (is_valid_number(buffer + i))
					array[pos] = ft_vector3(x, ft_atoi(buffer + i), y);
				pos++;
				++x;
			}
			status = 1;
		}
		else
			status = 0;
		++i;
	}
}

static t_vector3	*create_vector(int width)
{
	t_vector3	*l;
	int			i;

	l = (t_vector3*)ft_memalloc(sizeof(t_vector3) * (width));
	i = 0;
	while (i < width)
	{
		l[i] = ft_vector3(-1, -1, -1);
		++i;
	}
	return (l);
}

static void	fill_array(t_screen *screen, int fd)
{
	char	*buffer;
	int		line;
	t_vector3	**array;

	array = screen->vectors;
	line = 0;
	while (get_next_line(fd, &buffer))
	{
		array[line] = create_vector(screen->array_width);
		fill_line(array[line], buffer, 0, line);
		++line;
		free(buffer);
	}
}

void		get_vector_array(t_screen *screen, char *filename)
{
	int			fd;

	if ((fd = open(filename, O_RDONLY)) == -1)
		exit_error("Cannot open file.");
	get_number_of_vec(fd, &(screen->array_width), &(screen->array_height));
	screen->vectors = (t_vector3**)ft_memalloc((screen->array_height + 2) * sizeof(t_vector3*));
	close(fd);
	if ((fd = open(filename, O_RDONLY)) == -1)
		exit_error("Cannot open file.");
	fill_array(screen, fd);
}

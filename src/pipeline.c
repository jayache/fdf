/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/30 00:41:30 by jayache           #+#    #+#             */
/*   Updated: 2019/09/04 11:28:26 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** TRANSFORMATION PIPELINE
*/

static t_vector3	ndc_to_screen(t_screen *screen, t_vector3 vtc)
{
	t_vector3 scrn;

	scrn = vtc;
	scrn.x = vtc.x * (screen->width / 2);
	scrn.y = vtc.y * (screen->height / 2);
	return (scrn);	
}

static t_vector3	watch_vertex(t_screen *screen, t_vector4 world)
{
	t_vector4	cam;
	t_vector4	scrn;
	t_vector2	ndc;
	t_vector3	ret;

	cam = ft_vector4_p_matrix(world, screen->camera.view_matrix);
	scrn = ft_vector4_p_matrix(cam, screen->camera.projection_matrix);
	ndc = ft_vector2_normalize(ft_vector2(scrn.x, scrn.y));
	ret = ft_vector3(ndc.x, ndc.y, scrn.z);
	return (ret);
}

t_vector3	world_to_screen(t_screen *screen, t_vector4 world)
{
	t_vector3 temp;

	temp = watch_vertex(screen, world);
	return (ndc_to_screen(screen, temp));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pixel.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/30 00:18:55 by jayache           #+#    #+#             */
/*   Updated: 2019/08/30 00:19:01 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_pixel	ft_pixel(unsigned char red, unsigned char green, unsigned char blue)
{
	t_pixel	pixel;

	pixel.red = red;
	pixel.green = green;
	pixel.blue = blue;
	return (pixel);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_debug.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 10:08:15 by jayache           #+#    #+#             */
/*   Updated: 2019/09/04 10:58:55 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	print_tvectors(t_screen *s)
{
	int	x;
	int	y;

	y = 0;
	ft_printf("Transformed vectors:\n");
	while (y < s->array_height)
	{
		x = 0;
		while (x < s->array_width)
		{
			ft_printf("[%f ; %f ; %f]", s->tvectors[y][x].x,
					s->tvectors[y][x].y, s->tvectors[y][x].z);
			++x;
		}
		++y;
		ft_printf("\n");
	}
}

void	print_vectors(t_screen *s)
{
	int	x;
	int	y;

	y = 0;
	ft_printf("Normal vectors:\n");
	while (y < s->array_height)
	{
		x = 0;
		while (x < s->array_width)
		{
			ft_printf("[%f ; %f ; %f]", s->vectors[y][x].x, s->vectors[y][x].y, 
					s->vectors[y][x].z);
			++x;
		}
		++y;
		ft_printf("\n");
	}
}

void	test_gnv(t_screen *s)
{
	t_vector3	*vtc;
	int			lasty;

	lasty = 0;
	ft_printf("test gnv\n");
	while ((vtc = get_next_vector(s)))
	{
		if (vtc->z != lasty)
			ft_printf("\n");
		lasty = vtc->z;
		ft_printf("[%f ; %f ; %f]", vtc->x, vtc->y, vtc->z);
	}
	ft_printf("\ntest gnv tvectors\n");
	while ((vtc = get_next_tvector(s)))
	{
		ft_printf("[%f ; %f ; %f]\n", vtc->x, vtc->y, vtc->z);
	}
}
void	print_screen(t_screen *s)
{
	ft_printf("W: %d H: %d\n", s->array_width, s->array_height);
	print_vectors(s);
	print_tvectors(s);
	test_gnv(s);
}

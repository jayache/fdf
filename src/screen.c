/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/06 15:24:54 by jayache           #+#    #+#             */
/*   Updated: 2019/09/02 20:54:16 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		process_new_matrix(t_camera *camera)
{
	camera->origin_matrix = ft_matrix_translation(ft_vector3_opposite(
				camera->origin));
	camera->orient_matrix = ft_matrix_diag_symetrie(ft_matrix_ry(M_PI));
	camera->view_matrix = ft_matrix_product(camera->orient_matrix,
			camera->origin_matrix);
	camera->projection_matrix = ft_matrix_projection(camera->ratio, camera->near,
			camera->far, camera->fov);
}

t_camera	init_camera(unsigned short width, unsigned short height)
{
	t_camera camera;

	camera.fov = 60;
	camera.ratio = width / height;
	camera.near = 1;
	camera.far = 100;
	camera.origin = ft_vector3(1, 1, 1);
	process_new_matrix(&camera);
	return (camera);
}

/*
** CONSTRUCTOR OF T_SCREEN
** YEP, PLAYIN' IT LIKE I'M DOIN' CPP
*/

t_screen	init_screen(unsigned short width, unsigned short height)
{
	t_screen screen;

	screen.width = width;
	screen.height = height;
	if (!(screen.zbuffer = (long*)ft_memalloc(sizeof(long) * width * height)))
		exit(0);
	screen.mlx_ptr = mlx_init();
	screen.win_ptr = mlx_new_window(screen.mlx_ptr, width, height, "FDF");
	screen.img_ptr = mlx_new_image(screen.mlx_ptr, width, height);
	screen.screen = mlx_get_data_addr(screen.img_ptr, &(screen.bitsperpixel),
			&(screen.size_line), &(screen.endian));
	if (!screen.screen || !screen.mlx_ptr || !screen.win_ptr)
		exit(0);
	screen.camera = init_camera(width, height);
	return (screen);
}
